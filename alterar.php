<?php
require_once "classes/conexao.class.php";
require_once "sys/functions.php";
$con = new Conexao();

if(isset($_GET['id'])) $id = $_GET['id'];
if(isset($_GET['e'])) $erro = $_GET['e'];

if(isset($id) and $id <> ''){
   $sql = "SELECT * FROM tb_cadastro WHERE cd_id = " . $id;
   $resul = $con->Buscar($sql);
}
else {
    header("Location: listagem.php?e=1");
}
if(isset($erro) and $erro <> ""){
    switch ($erro) {
        case '1':
           echo "Os dados precisam estar preenchidos.";
            break;
        case '2':
            echo "Dados atualizados corretamente.";
            break;
        case '3':
            echo "Data está incorreta ou incoerente.";
            break;
        case '4':
            echo "Email inválido ou incoerente.";
            break;
        case '5':
            echo "A foto não tem uma extensão permitida.";
            break;
        case '6':
            echo "Você não passou no teste de CAPTCHA.";
            break;
    }
}

?>
<!DOCTYPE>
<html>
<meta charset="utf-8">
<title>Tela de Alterar Cadastro</title>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="js/chosen/chosen.css" type="text/css" />
    <link rel="stylesheet" href="js/tagedit/css/jquery.tagedit.css" type="text/css"/>
    <?php include "includes/jqueryui.php" ; ?>
    <script type="text/javascript" src="js/jquery.Jcrop.js"></script>
    <script type="text/javascript" src="js/actions.js"> </script>
    <script type="text/javascript" src="js/chosen/chosen.jquery.js"> </script>
    <script type="text/javascript" src="js/previewImg.js"></script>
    <script type="text/javascript" src="js/tagedit/js/jquery.autoGrowInput.js"></script>
    <script type="text/javascript" src="js/tagedit/js/jquery.tagedit.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body>
<?php require_once("includes/menu.php");
      include "includes/thumb.php"; 
    if(!empty($resul)):
?>

<h3>Alterar Cadastro</h3>
    <form name="cadastro" id="alteracao_form" method="post" action="actions/alterarDados.php?id=<?php echo $resul[0]['cd_id'];?>" onSubmit="return ValidarCadastro()" enctype="multipart/form-data">
        <p>Nome de usuário:</p>
        <input type="text" name="username" placeholder="Nome do usuário..." maxlength="60" value="<?php echo utf8_encode($resul[0]['nm_nome']) ;?>" >
        <p>Email:</p>
        <input type="email" name="useremail" placeholder="fulano@email.com" pattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" 
         value="<?php echo $resul[0]['nm_email']; ?>"  maxlength="60"> 
        <p>Data de nascimento:</p>
        <input type="text" id="datepicker" name="userbday" placeholder="DD/MM/AAAA" value="<?php $resdata = converteDataRecebida($resul[0]['dt_nascimento']); echo $resdata;?>" maxlength="10">
        <p>Foto (upload):</p>        
        <input type="file" id="file" accept="image/*" name="userphoto">
        <?php 
            if ($resul[0]['nm_url_foto'] <> ""):
                echo "<img src='files/images/usuarios/" . $id . "/thumb/" . $resul[0]['nm_url_foto']. "'>" ;
                echo "<input type='hidden' name='useroldphoto' value='" . $resul[0]['nm_url_foto']. "'>" ;
            else:
                echo "<br><strong>Usuário ainda não tem foto.</strong><br>";
            endif; 
        ?>
        
        <br>
        <p>Categoria:</p>
        <select data-placeholder="Escolha sua categoria..." multiple class="chosen-select" name="usercategory[]" id="usercategory" style="width:200px;">

            <?php
                $sqlcat = "SELECT nm_nome, cd_id FROM tb_categoria";
                $resulcat = $con->Buscar($sqlcat);
                $sqlcatusuario = "SELECT r.cd_id_categoria FROM tb_res_categoria r INNER JOIN tb_cadastro c 
                                    WHERE r.cd_id_cadastro = $id AND r.cd_id_cadastro = c.cd_id";
                $buscaCatUsuario = $con->Buscar($sqlcatusuario);

                if(!empty($resulcat)):
                    foreach ($resulcat as $res) {
                        echo "<option value='{$res['cd_id']}'" ;
                            if(noresultset($res['cd_id'], $buscaCatUsuario, 'cd_id_categoria')):
                                echo " selected";
                            endif;
                        echo ">" . utf8_encode($res['nm_nome']) . "</option>";
                     }
                     
                else:
                    echo "<option value='0' selected>Selecione uma categoria</option>";
                endif;
            ?>
        </select>

        <p>Tipos - subtipos:</p>
        <select data-placeholder="Escolha seu tipo..." multiple tab-index="-1" class="chosen-select" name="usersubtype[]" id="usersubtype" style="width:300px;">
    <?php 
        
        $sql_tb_tipo = "SELECT sub.cd_id as codigo_subtipo, sub.nm_nome as nome_subtipo, tipo.nm_nome as nome_tipo ";
        $sql_tb_tipo .= "FROM tb_subtipo sub INNER JOIN tb_tipo tipo WHERE tipo.cd_id = sub.cd_tipo";
        $buscaSubTipo = $con->Buscar($sql_tb_tipo);
        $sql_subtipo_usuario = "SELECT cd_id_cadastro as cadastro, cd_id_subtipo as subtipo FROM tb_res_subtipo WHERE cd_id_cadastro = $id";
        $buscaSubtipoUsuario = $con->Buscar($sql_subtipo_usuario);
        //se a busca por subtipos do banco não estiver vazia
        if(!empty($buscaSubTipo)){
            //cada linha é percorrida
            foreach ($buscaSubTipo as $res) {
                //caso nao tenha sido iniciado ou o nome_tipo seja diferente do ultimo
                if(!isset($nm_tipo) or $res['nome_tipo'] !== $nm_tipo) {
                    //a variavel nm_tipo é atribuida
                    $nm_tipo = $res['nome_tipo'];
                    if(!isset($nm_tipo))
                        echo "<optgroup label='".$nm_tipo."'> ";
                    else
                        echo "</optgroup>";
                        echo "<optgroup label='".$nm_tipo."'> ";
                }
                //as opções são preenchidas de acordo com o código do subtipo
                echo "<option value='" . $res["codigo_subtipo"] ."'";
                    //caso o codigo do subtipo seja algum dos já atribuidos ao usuario 
                    if(noresultset($res['codigo_subtipo'], $buscaSubtipoUsuario, 'subtipo')):
                        echo " selected";
                    endif;
                echo ">". utf8_encode($res["nome_subtipo"]) . "</option>";
            }
        }
    ?>
        </select>

         <p>Tags:</p>
        <p>
        <input type="hidden" name="tag[]" value="" class="tag"/>
    <?php 
        $sqlTagsUsuario = "SELECT res.cd_id_tag as codigo_tag, tag.nm_nome as nome_tag FROM tb_res_tag res INNER JOIN tb_tag tag WHERE res.cd_id_cadastro = ".$id." AND res.cd_id_tag = tag.cd_id";
        $tagsUsuario = $con->Buscar($sqlTagsUsuario);

        if(!empty($tagsUsuario)):
        foreach ($tagsUsuario as $key) {
            echo '<input type="hidden" name="tag['.$key['codigo_tag'] .'-a]" value="'.utf8_encode($key['nome_tag']).'" class="tag"/>';
        }
        endif;
    ?>
            
        </p>
        <input type="hidden" name="usertypeNum" id="usertypeNum" value='1'>
        <input type="hidden" id="x" name="x" />
        <input type="hidden" id="y" name="y" />
        <input type="hidden" id="w" name="w" />
        <input type="hidden" id="h" name="h" />

        <br>
       <?php include("includes/captcha.php"); ?>
        <input type="submit" value="Alterar">       
    </form>
	
	<div id="erro">

	</div>
<?php
    else:
        echo "Nenhum usuário encontrado.";
    endif;
?>

    <script>
    
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Desculpe, nenhuma opção encontrada!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      $(selector).chosen(config[selector]);
    }
    $("#alteracao_form").find('input.tag').tagedit({
        //autocompleteURL: 'js/tagedit/server/autocomplete.php',
        autocompleteURL: 'actions/autocompletetags.php'
    });

    </script>
</body>
</html>