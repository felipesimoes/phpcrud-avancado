<?php
if(isset($_GET["e"])):
$erro = $_GET["e"];
endif;
if (isset($erro) and ($erro != "")):
    echo "Houve um erro não identificado";
endif;

?>
<!DOCTYPE>
<html>
<meta charset="utf-8">
<title>Tela de Criar Categoria</title>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/actions.js"> </script>
</head>
<body>
<?php require_once "includes/menu.php"; ?>

<h3>Criar Categoria</h3>
    <form name="alteracao" id="alteracao_form" method="post" action="actions/criarcategoria.php" onSubmit="return ValidarNovaCategoria()">
        <p>Nome da categoria:</p>
        <input type="text" name="categoryname" placeholder="Nome da categoria..." maxlength="50" >
       
        <br>
        <input type="submit" value="Criar Categoria">       
    </form>
	
	<div id="erro">
		
	</div>
</body>
</html>