<?php
require_once "classes/conexao.class.php";
$con = new Conexao();

if(isset($_GET['id'])){
    $id = $_GET['id'];
    $id = trim($id);
}
if(isset($_GET['e'])) $erro = $_GET['e'];

if(isset($id) and !empty($id)):
    $catsql = "SELECT * FROM tb_categoria WHERE cd_id = {$id}";
    $buscacat = $con->Buscar($catsql);

else:
    header("location: categorias.php");

endif;

?>
<!DOCTYPE>
<html>
<meta charset="utf-8">
<title>Tela de Alterar Categoria</title>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/actions.js"> </script>
</head>
<body>
<?php  
require_once "includes/menu.php";
if(!empty($buscacat)):
?>
<h3>Alterar Categoria</h3>
    <form name="alteracao" id="alteracao_form" method="post" action="actions/alterarcategoria.php?id=<?php echo $id; ?>" >
        <p>Nome da categoria:</p>
        <input type="text" name="categoryname" placeholder="Nome da categoria..." maxlength="50" value="<?php echo utf8_encode($buscacat[0]['nm_nome']); ?>" >
       
        <br>
        <input type="submit" value="Alterar Categoria">       
    </form>
	
	<div id="erro">
		
	</div>
<?php
    else:
        echo "Erro na busca pelo id da categoria";
    endif; 
?>
</body>
</html>