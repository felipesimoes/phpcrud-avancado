<?php
require_once "classes/conexao.class.php";
$con = new Conexao();

if(isset($_GET['e'])) $erro = $_GET['e'];

if(isset($erro)){
    switch ($erro) {
        case '1':
            echo "Problema ao alterar/criar uma categoria.";
            break;
        case '2':
            echo "Categoria alterada.";
            break;            
        case '3':
            echo "Não pode deixar o nome da categoria em branco.";
            break;        
        case '4':
            echo "Tentou excluir uma categoria inexistente.";
            break;
        case '5':
            echo "Tentou excluir uma categoria inexistente ou não permitida.";
            break;
        case '6':
            echo "Ocorreu um erro na tentativa de excluir a categoria.";
            break; 
        case '7':
            echo "A categoria foi excluida com sucesso.";
            break;     
        case '8':
            echo "A categoria foi criada com sucesso.";
            break;      
    }
}
?>
<!DOCTYPE>
<html>
<meta charset="utf-8">
<title>Tela de Categorias</title>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <script type="text/javascript" src="js/actions.js"> </script>
    <style>
    table,td {
        border: 1px solid black;
        text-align: center;
        margin-top: 30px;
        margin-left: 20px;
    }
    </style>
</head>
<body>
<?php include "includes/menu.php"; 


    $sqlcat = "SELECT * FROM tb_categoria";
    $resulcat = $con->Buscar($sqlcat);
    if(!empty($resulcat)):
?>
    <h2>Listagem de Categorias</h2>
    <div id="novacategoria">
        <a href="criarcategoria.php">Criar nova categoria</a>
    </div>
    <table border="1" style="width:800px">
    <thead>
        <tr>
            <td>Nome</td>
            <td>Alterar</td>
            <td>Excluir</td>
        </tr>
    </thead>
    <tbody>
<?php
        foreach ($resulcat as $cat) {
            echo "<tr>";
            echo "<td>" . utf8_encode($cat['nm_nome']) . "</td>";
            echo "<td><a href='alterarcategoria.php?id=". $cat['cd_id'] ."'> X </a> </td>";
            if($cat['cd_id'] <> 1){
                echo "<td><a href='actions/excluircategoria.php?id=" . $cat['cd_id'] ."'> X </a> </td>";
            }
            else {
                echo  "<td> - </td>";
            }
            echo "</tr>";
        }

?>
    </tbody>
    </table>
<?php
    else:
        echo "Problema na busca por categorias.";
    endif;
?>
</body>
</html>