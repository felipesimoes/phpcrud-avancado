<?php
require_once "classes/conexao.class.php";
require_once "sys/functions.php";
$con = new Conexao();

if(isset($_GET['id'])) $id = $_GET['id'];

if(isset($id)){

	$sql = "SELECT nm_nome, nm_email, dt_nascimento, nm_url_foto FROM tb_cadastro WHERE cd_id = " . $id;
	$resul = $con->Buscar($sql);
}
else {
	header("Location: listagem.php?e=1");
}

?>

<!DOCTYPE>
<html>
<meta charset="utf-8">
<title>Tela de Visualização</title>
<head>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<style type="text/css">
		#foto_usuario {
			width: 30%;
		}
	</style>
</head>
<body>
<?php include("includes/menu.php");
	if(!empty($resul)):
?>	
<h2>Dados do usuário:</h2>
<?php
		foreach ($resul as $row) {
			echo "Nome: " . utf8_encode($row['nm_nome']) ."<br>";
			echo "Email: " . $row['nm_email'] . "<br>";
			echo "Data de Nascimento: " . converteDataRecebida($row["dt_nascimento"]) . "<br>";
			if(trim($row['nm_url_foto']) <> ""){
				$src = "files/images/usuarios/".$id . "/" . $row['nm_url_foto'];
				$srcthumb = "files/images/usuarios/" . $id . "/thumb/" . $row['nm_url_foto'];
				echo "<img src='$src' style='width:30%;'><br>";
				echo "<img src='$srcthumb'>";
			}else {
				echo "Usuario sem foto";
			}
		}
	else:
		header("Location: listagem.php?e=1");
	endif;
?>

</body>
</html>