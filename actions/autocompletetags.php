<?php
require_once "../classes/conexao.class.php";
require_once "../sys/functions.php";
$con = new Conexao();

$buscatags = $con->Buscar("SELECT * FROM tb_tag t");

foreach ($buscatags as $key) {
    $autocompletiondata[$key["cd_id"]] = utf8_encode($key["nm_nome"]);
}
if(isset($_GET['term'])) {
    $result = array();
    foreach($autocompletiondata as $key => $value) {
        if(strlen($_GET['term']) == 0 || strpos(strtolower($value), strtolower($_GET['term'])) !== false) {
            $result[] = '{"id":'.$key.',"label":"'.$value.'","value":"'.$value.'"}';
        }
    }
    
    echo "[".implode(',', $result)."]";
}
?>