    <?php
    
    function criarTipo($c) {
        echo '<p>#'.$c . ' Tipo:</p>';
        echo '<select data-placeholder="Escolha seu tipo..." class="chosen-select" name="usertype'.$c.'[]" id="usertype'.$c.'" style="width:200px;">';
        require_once "../classes/conexao.class.php";
        $con = new Conexao();

        $sql_tb_tipo = "SELECT nm_nome, cd_id FROM tb_tipo ";
        $resul = $con->Buscar($sql_tb_tipo);

        if(!empty($resul)){
            foreach ($resul as $res) {
                echo "<option value='" . $res["cd_id"] .  "'>". utf8_encode($res["nm_nome"]) . "</option>";
            }
        }
        echo '</select>';

        echo "<select data-placeholder='Escolha seus subtipos...' multiple class='chosen-select subtipo-select' id='usersubtype".$c."' name='usersubtype". $c ."[]' style='width:200px;'>";
        $sql_tb_subtipo = "SELECT * FROM tb_subtipo t WHERE cd_tipo = ". 2;
        $resul = $con->Buscar($sql_tb_subtipo);

        if(!empty($resul)){
            foreach ($resul as $res) {
                echo "<option value='" . $res["cd_id"] .  "'>". utf8_encode($res["nm_nome"]) . "</option>";
            }
        }
        echo "</select>";
    }
    if (isset($_GET['qtd'])) {
        $qtd = $_GET["qtd"];
    }else { $qtd = 1; }
  
    echo criarTipo($qtd);

    ?>
    <input type="hidden" name="usertypeNum" value='<?php echo $qtd;?>'>

