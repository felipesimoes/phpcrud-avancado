<?php
	require_once "../classes/conexao.class.php";
	require_once "../sys/functions.php";
	$con = new Conexao();

	if(isset($_GET['pesquisa'])) $pesquisa = $_GET['pesquisa'];

	$sql = "SELECT nm_nome as Nome, dt_nascimento as 'Data de Nascimento', cd_id as 'Código' FROM tb_cadastro ";

	$sql.= "WHERE nm_nome LIKE '%" . $pesquisa ."%'"; 
	
	if(isset($_GET['ctg'])) {
		$ctg = $_GET['ctg'];
	}else {
		$ctg = 'nm_nome';
	}
		

	$prioridade = " AND ic_ativo = 1 ORDER BY ". $ctg ." DESC";
	$sql .= $prioridade;
 
	$busca = $con->Buscar($sql);
	$resul = $con->Buscar($sql);
	if(!empty($resul)):
?>
	
	<p style="font-size:14px;margin-top:10px;margin-left:15px;">Clique no nome para ordenar de acordo</p>
	<table id="myTable" class="tablesorter" style="max-width:80%;font-size:15px;">
	
	<thead>
		<tr>
			<th>Nome</th>
			<th>Data de Nascimento</th>
			<th>Id</th>
			<th>Alterar</th>
			<th>Visualizar</th>
			<th>Excluir</th>
			<th>Notificar</th>
		</tr>
	</thead>
	<tbody>
	<?php
		foreach ($resul as $row) {
			echo "<tr>";
			echo "<td>".utf8_encode($row['Nome'])."</td>";
			echo "<td>".converteDataRecebida($row['Data de Nascimento'])."</td>";
			echo "<td>".$row['Código']."</td>";
			echo "<td><a href='alterar.php?id={$row['Código']}'>X</a></td>";
			echo "<td><a href='visualizar.php?id={$row['Código']}'>X</a></td>";
			echo "<td><a href='actions/excluir.php?id={$row['Código']}'>X</a></td>";
			echo "<td><a href='actions/notificar.php?id={$row['Código']}'>X</a></td>";
			echo "</tr>";
		}
	?>
	</tbody>
	</table>
	<div id="exportbuttons">
		<a href="#" onclick="$('#myTable').tableExport({type:'excel',escape:'false',ignoreColumn: [3,4,5,6]});">Export XLS</a>
		<a href="#" onclick="$('#myTable').tableExport({type:'txt',escape:'false',ignoreColumn: [3,4,5,6]});">Export TXT</a>
	</div>
	<script>
	     $(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
    } 
); </script>
<?php
	else:
		echo "Sem usuários cadastrados.";
	endif;
?>

