<?php
require_once "../classes/conexao.class.php";
$con = new Conexao();

if(isset($_GET['id'])) {
	$id = $_GET['id'];
	$id = trim($id);
}
//Caso haja um id, que não esteja vazio e tenha valor diferente de 1 ou 0
// 1 e 0 são valores padrões das categorias
if(isset($id) and !empty($id) and $id <> 1 and $id <> 0){
	//uma busca é feita pelos dados da id passada
	$pesquisasql = "SELECT * FROM tb_categoria WHERE cd_id = {$id}";
	$resulpesquisa = $con->Buscar($pesquisasql);
	//caso haja retorno da busca há uma id a ser apagada no banco
	if(!empty($resulpesquisa)){

		//altera todos os usuários que tinham essa categoria para uma padrão
		$updsql = "UPDATE tb_cadastro SET cd_categoria = 1 WHERE cd_categoria = " . $id;
		$updatecategoria = $con->Alterar($updsql); 

		//após a confirmação, os dados serão excluidos para este id
		$delsql = "DELETE FROM tb_categoria WHERE cd_id = {$id}";
		$excluircategoria = $con->Alterar($delsql);
		//caso a exclusão tenha ocorrido de modo certo
		if($excluircategoria){
			header("location: ../categorias.php?e=7");
		}else{
			header("location: ../categorias.php?e=6");
		}	
	}
	//caso não haja dados para a busca, significa que a id não existe no banco
	else {
	header("location: ../categorias.php?e=4");
	}

}
//caso nao tenha id ou seja um id padrão não é permitida sua exclusão
else {
	header("location: ../categorias.php?e=5");
}

?>