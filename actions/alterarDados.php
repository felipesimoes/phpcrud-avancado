<?php

require_once "../classes/conexao.class.php";
require_once "../sys/functions.php";
$con = new Conexao();

if(isset($_GET['id'])) $id = $_GET['id'];
//caso tenha um id passado pela url
if(isset($id)){
   	//recebe por post do form
	$nome = $_POST["username"];
	$email = $_POST["useremail"];
	$bday = $_POST["userbday"];
	$categ = $_POST["usercategory"];
	$subtipo = $_POST["usersubtype"];
	$recaptcha = $_POST["g-recaptcha-response"];

	if($recaptcha != true){
	header("location: ../alterar.php?id={$id}&e=6");
	exit;
	}
	//caso o usuario tente enviar uma foto
	if(isset($_FILES["userphoto"]["name"])){
		$foto = $_FILES["userphoto"]["name"];
	}
	//caso contrario foto é declarada string vazia
	else { $foto = ""; }
	//caso nome, email e data de nascimento nao estejam em branco
	if(!empty($nome) and !empty($email) and !empty($bday) and !empty($categ) and !empty($subtipo) ) {
		//o campo de sql referente a foto é declarado vazio (nao interfere na query)
		$fotosql = "";
		
		//o email é validado
		//caso não esteja correto o erro é transmitido a tela anterior
		if(filter_var($email, FILTER_VALIDATE_EMAIL) == FALSE) {
			header("location: ../alterar.php?id={$id}&e=4");
			exit;
		}
		//faz-se a validacao da data de nascimento
		if(validaData($bday)){
			//caso seja validada a data é convertida para a forma adequada de inserção no banco
			$bday = converteData($bday);
		}
		//caso a data de nascimento não esteja correta
		else {
			header("location: ../alterar.php?id={$id}&e=3");
			exit;
		}
		

		//caso o usuario tenha tentado enviar uma imagem nova
		if( (isset($_FILES["userphoto"]["name"])) and ($_FILES["userphoto"]["name"] <> "") ) {

			$allowedExts = array("gif", "jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["userphoto"]["name"]);
			$extension = end($temp);
			//caso a imagem tenha um tipo permitido segue-se o cadastro
			if ((($_FILES["userphoto"]["type"] == "image/jpeg")
				|| ($_FILES["userphoto"]["type"] == "image/jpg")
				|| ($_FILES["userphoto"]["type"] == "image/png"))
				&& in_array($extension, $allowedExts)) {
					//a variavel recebe o nome da foto
					$foto = $_FILES["userphoto"]["name"];
			}
			// se a imagem nao for permitida retorna um erro avisando
			else {
				header("Location: ../alterar.php?id={$id}&e=5");
				exit;
			}
		}

		//caso o usuario esteja enviando uma foto nova
		if($foto <> "") { 

			require_once "../sys/paths.php";
			//o nome da foto deve ser o id do usuário 
			$foto = $id . "." . $extension;
			//o campo de foto será atualizado
			$fotosql = ", nm_url_foto = '$foto'" ; 
			$pastauser = $cfg_usuariospath . $id;

			//busca pela nome da foto antiga do usuario 
			$buscafoto = "SELECT nm_url_foto FROM tb_cadastro WHERE cd_id = ". $id;
			$resbuscafoto = $con->Buscar($buscafoto);
			//caso o usuário tenha uma foto pré-cadastrado ela é atualizada
			if(!empty($resbuscafoto)) { $fotoantiga = $resbuscafoto[0]['nm_url_foto']; }
			
			//caso o usuario tenha uma foto antiga, ela e seu thumb precisam ser apagados
			if(isset($fotoantiga) and trim($fotoantiga) <> "") {
				$fotoapagada = $pastauser . "/" . $fotoantiga;
				$thumbapagada = $pastauser . "/thumb/" . $fotoantiga;  
				//checa se as fotos estao nos diretorios para serem apagadas
				if(is_dir($fotoapagada)){
					unlink($fotoapagada);
					unlink($thumbapagada);
				}
			}

			/** Caso o diretorio nao exista, os diretorios sao criados */
		   	if(!is_dir($pastauser)){
		   		mkdir($pastauser, 0777, true);
		   		mkdir($pastauser . "/thumb", 0777, true);
		   	}

			//move fotos novas
			move_uploaded_file($_FILES["userphoto"]["tmp_name"], $pastauser . "/". $foto);
			$targ_w = 250;
            $targ_h = 170;
            $jpeg_quality = 90;
            $src = $pastauser . "/". $foto; 
            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

            switch ($extension) {
            	case 'jpg':
            		$img_r = imagecreatefromjpeg($src);
			        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
			        $targ_w,$targ_h,$_POST['w'],$_POST['h']);
			        imagejpeg($dst_r, $pastauser . "/thumb/" . $foto , $jpeg_quality);
	        		break;
            	
            	case 'png':
            		$img_r = imagecreatefrompng($src);
			        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
			        $targ_w,$targ_h,$_POST['w'],$_POST['h']);
			        imagepng($dst_r, $pastauser . "/thumb/" . $foto , $jpeg_quality);
            		break;
            }			
		}

		$nome = utf8_decode($nome);
		$email = utf8_decode($email);
		//Forma-se a query a ser atualizada
		$sqlupdate = "UPDATE tb_cadastro SET nm_nome = '$nome'" .
			", nm_email = '$email'" .
			", dt_nascimento = '$bday'" .
			$fotosql .
			" WHERE cd_id = " . $id;
		
		$con->Alterar($sqlupdate);

		//apaga os registros relacionados aquele usuário na tb_res_categoria
		$delsql = "DELETE FROM tb_res_categoria WHERE cd_id_cadastro = $id";
		$con->Alterar($delsql);

		//forma a string de categorias que o usuário escolheu e insere na tabela de resolução
		$insertCatSQL = "INSERT INTO tb_res_categoria (cd_id_cadastro, cd_id_categoria) VALUES ";

		foreach ($_POST['usercategory'] as $value) {
			$insertCatSQL .= "($id, $value),";
		}
		//remove a última vírgula e faz a inserção na tabela
		$insertCatSQL = substr($insertCatSQL, 0, -1);
		$insertCategorias = $con->Alterar($insertCatSQL);

		//apaga os registros relacionados aquele usuário na tb_res_subtipo
		$delsql = "DELETE FROM tb_res_subtipo WHERE cd_id_cadastro = $id";
		$con->Alterar($delsql);

		//forma a string de subtipos que o usuário escolheu e insere na tabela de resolução
		$insertSubtipoSQL = "INSERT INTO tb_res_subtipo (cd_id_cadastro, cd_id_subtipo) VALUES ";

		foreach ($subtipo as $value) {
			$insertSubtipoSQL .= "($id, $value),";
		}
		//remove a última vírgula e faz a inserção na tabela
		$insertSubtipoSQL = substr($insertSubtipoSQL, 0, -1);
		$insertSubtipos = $con->Alterar($insertSubtipoSQL);


		/*
		/ Busca os valores de inputs tag e compara com os valores já existentes no banco
		/ $resulttags armazena outros arrays, separados por informações deletadas, alteradas, inalteradas e novas
	   	*/
	   
	   	$showResult = false;

		if( (array_key_exists('tag', $_POST) || array_key_exists('formdata', $_POST))) {
		    // Include the autocompleteScript to know what was in the database
		    require_once ('autocompletetags.php');
		    $resulttags = array('new' => array(), 'deleted' => array(), 'changed' => array(), 'not changed' => array());
		    $tags = array_key_exists('tag', $_POST)? $_POST['tag'] : $_POST['formdata']['tags'];
		    $showResult = false;

		    foreach($tags as $key => $value) {
		        if(preg_match('/([0-9]*)-?(a|d)?$/', $key, $keyparts) === 1) {
		            $showResult = true;
		            if(isset($keyparts[2])) {
		                switch($keyparts[2]) {
		                    case 'a':
		                        if($autocompletiondata[$keyparts[1]] != $value) {
		                            // Items has changed
		                            $resulttags['changed'][] = $keyparts[1] . ' (new value: "' . $value . '")';
		                        }
		                        else {
		                            $resulttags['not changed'][] = $keyparts[1] ;
		                        }
		                        break;
		                    case 'd':
		                        $resulttags['deleted'][] = $keyparts[1] . ' ("' . $value . '")';
		                        break;
		                }
		            }
		            else {
		                $resulttags['new'][] = $value;
		            }
		        }
		    }
		}
		//caso tenhas tags diferentes das já registradas
		$sqlDeleteTagsUsuario = "DELETE FROM tb_res_tag WHERE cd_id_cadastro = " . $id;
		$AntigasTagsDeletas = $con->Alterar($sqlDeleteTagsUsuario);

		if(count($resulttags['new']) > 0) {
		// As tags novas são incluídas no banco
		// Depois seus cd_id são devolvidos
			$sqlInsertNewTags =  "INSERT INTO tb_tag (nm_nome) VALUES " ;
				foreach ($resulttags['new'] as $key => $value) {
					$sqlInsertNewTags .= "('". utf8_decode($value) . "')," ;
				}
			// Remove última vírgula
			$sqlInsertNewTags = substr($sqlInsertNewTags, 0, -1);
			$InsertTags = $con->Alterar($sqlInsertNewTags);

			$sqlRetrieveNewTagsIds = "SELECT cd_id FROM tb_tag WHERE ";
				foreach ($resulttags['new'] as $key => $value) {
					$sqlRetrieveNewTagsIds .= " nm_nome = '". $value."' OR" ;
				}
			$sqlRetrieveNewTagsIds = substr($sqlRetrieveNewTagsIds, 0, -3); //Remove o AND da última inserção
		    $NewTagsIds = $con->Buscar($sqlRetrieveNewTagsIds);

		// As tags com os nomes passados são recuperadas e armazenadas em um array
		}
			print_r($resulttags['not changed']);

			foreach ($resulttags['not changed'] as $key => $value) {
				$tagsIds[$key] = $value;
			}
				if(count($resulttags['new']) > 0) {
					foreach ($NewTagsIds as $key) {
			            $tagsIds[$key['cd_id']] = $key['cd_id'];
			        }
			    }
			    print_r($tagsIds);
			$sqlInsertUserTags = "INSERT INTO tb_res_tag (cd_id_cadastro,cd_id_tag) VALUES ";
	        foreach ($tagsIds as $key => $value) {
	            $sqlInsertUserTags .= "(".$id.",".$value ."),";
	        }
	        $sqlInsertUserTags = substr($sqlInsertUserTags, 0, -1);
	        $insereTagsUsuario = $con->Alterar($sqlInsertUserTags);	      
		echo $sqlInsertUserTags;


		header("Location: ../alterar.php?id={$id}&e=2");
		exit;
	//dados não estão preenchidos corretamente
	} else {
		header("Location: ../alterar.php?id={$id}&e=1");
		exit;
	}
}


else {
	header("Location : ../alterar.php?id={$id}&e=2");
	exit;
}



?>