<?php
require_once("../classes/conexao.class.php");
$con = new Conexao();

if(isset($_GET['id'])) {
	$id = $_GET['id'];
	$id = trim($id);
}

//caso haja um id válido
if(isset($_GET['id']) and !empty($id)) {
	$sqlbuscacadastro = "SELECT nm_email FROM tb_cadastro WHERE cd_id = {$id}";
	$buscaemail = $con->Buscar($sqlbuscacadastro);
	if(!empty($buscaemail)){
		$email = $buscaemail[0]['nm_email'];
		$emailpadrao = "felipe.lima@kbrtec.com.br";
		$assunto = "Notificação";
		$mensagem = "O usuário de email $email está sendo notificado.";
		$headers = "From: teste@teste.com";
		mail($emailpadrao, $assunto, $mensagem, $headers);
		header("Location: ../listagem.php?e=2");
	}else{
		header("Location: ../listagem.php?e=1");
	}
}
//não tem id ou id em branco
else {
	header("Location: ../listagem.php?e=3");
}

?>