<?php
require_once "../classes/conexao.class.php";
require_once "../sys/paths.php";
require_once "../sys/functions.php";
$con = new Conexao();

if(isset($_GET['id'])) {
	$id = $_GET['id'];
	$id = trim($id);
} 
if(!empty($id)){
	//O cadastro é removido do banco de dados
	$sqldelete = "UPDATE tb_cadastro SET ic_ativo = 0 WHERE cd_id = {$id}";
	$excluircadastro = $con->Alterar($sqldelete);

	//passa o diretorio da pasta com imagens do usuário
	$diretorio_usuario = $cfg_usuariospath . $id;

	//Excluir diretorio com id do usuário
	if(is_dir($diretorio_usuario)){
		rmdir_recursive($diretorio_usuario);
	}

	//Caso o cadastro tenha sido excluido com sucesso
	//Retorna para a listagem de cadastros
	if($excluircadastro){
		header("location: ../listagem.php?e=5");
	} 
	else{
		header("location: ../listagem.php?e=4");
	}
}
		
else{
	header("location: ../listagem.php?e=6");
}
	
?>