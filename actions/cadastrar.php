<?php

$nome = $_POST["username"];
$email = $_POST["useremail"];
$bday = $_POST["userbday"];
$categ = $_POST["usercategory"];
$subtipo = $_POST["usersubtype"];
$recaptcha = $_POST["g-recaptcha-response"];

require_once "../sys/functions.php";

if($recaptcha != true){
	header("Location: ../cadastro.php?e=7");
	exit;
}

if( !empty($nome) and !empty($email) and !empty($bday) and !empty($categ) and !empty($subtipo)) {

	if(validaData($bday)){
		$bday = converteData($bday);
	}
	else{
		header("Location: ../cadastro.php?e=6");
		exit;
	}

	if(filter_var($email, FILTER_VALIDATE_EMAIL) == TRUE) {

		//caso o usuario tenha tentado enviar uma imagem
		if( (isset($_FILES["userphoto"]["name"])) and ($_FILES["userphoto"]["name"] <> "") ) {

			$allowedExts = array("jpeg", "jpg", "png");
			$temp = explode(".", $_FILES["userphoto"]["name"]);
			$extension = end($temp);
			//caso a imagem tenha um tipo permitido segue-se o cadastro
			if ((($_FILES["userphoto"]["type"] == "image/jpeg")
				|| ($_FILES["userphoto"]["type"] == "image/jpg")
				|| ($_FILES["userphoto"]["type"] == "image/png"))
				&& in_array($extension, $allowedExts)) {
				$foto = $_FILES["userphoto"]["name"];
			}
			// se a imagem nao for permitida retorna um erro avisando
			else {
				header("Location: ../cadastro.php?e=5");
				exit;
			}
		}

		require_once "../classes/conexao.class.php";
		$conn = new Conexao();


		$novoCadastroSQL = "INSERT INTO tb_cadastro (nm_nome, nm_email, dt_nascimento) VALUES ('" .
				$nome . "','" . $email . "','" . $bday . "')";

		$insertUser = $conn->Alterar($novoCadastroSQL);


		$buscaUserId = "SELECT MAX(cd_id) as id FROM tb_cadastro WHERE nm_email = '". $email . "'";
		$selectUserId = $conn->Buscar($buscaUserId);
		$userId = $selectUserId[0]["id"];

		//forma a string de categorias que o usuário escolheu e insere na tabela de resolução
		$insertCatSQL = "INSERT INTO tb_res_categoria (cd_id_cadastro, cd_id_categoria) VALUES ";

		foreach ($categ as $value) {
			$insertCatSQL .= "($userId, $value),";
		}
		//remove a última vírgula e faz a inserção na tabela
		$insertCatSQL = substr($insertCatSQL, 0, -1);
		$insertCategorias = $conn->Alterar($insertCatSQL);

		//forma a string de subtipos que o usuário escolheu e insere na tabela de resolução
		$insertSubtipoSQL = "INSERT INTO tb_res_subtipo (cd_id_cadastro, cd_id_subtipo) VALUES ";

		foreach ($subtipo as $value) {
			$insertSubtipoSQL .= "($userId, $value),";
		}
		//remove a última vírgula e faz a inserção na tabela
		$insertSubtipoSQL = substr($insertSubtipoSQL, 0, -1);
		$insertSubtipos = $conn->Alterar($insertSubtipoSQL);


		if(isset($foto)){
			$foto = $userId .".". $extension;
			$updatesqlfoto = "UPDATE tb_cadastro SET nm_url_foto = '{$foto}' WHERE cd_id = {$userId}";
			$updfoto = $conn->Alterar($updatesqlfoto);
		}
		

		/** Envia notificacao de cadastro ao usuario recem-cadastrado */
			$emailpadrao = "felipe.lima@kbrtec.com.br";
			$assunto = "Novo Cadastro";
			$mensagem = "O usuário de email $email está sendo notificado do cadastro.";
			$headers = "From: teste@teste.com";
			$envio = mail($emailpadrao, $assunto, $mensagem, $headers);


		/** Cria os diretorios com o id do usuario e um thumb dentro do mesmo */
		require_once "../sys/paths.php";
		$pastauser = $cfg_usuariospath . $userId;
		/** Caso o diretorio nao exista, os diretorios sao criados */
	   	if(!is_dir($pastauser)){
	   		mkdir($pastauser, 0777, true);
	   		mkdir($pastauser . "/thumb", 0777, true);
	   	}
	   	/** Caso o usuario esteja enviando uma foto */
	   	if($foto <> ""){
	   		/** O arquivo é enviado para o diretorio 
	   		O arquivo é redimensionado e enviado para a pasta thumb */
			move_uploaded_file($_FILES["userphoto"]["tmp_name"], $pastauser . "/". $foto);


			$targ_w = 250;
            $targ_h = 170;
            $jpeg_quality = 90;
            $src = $pastauser . "/". $foto; 
            $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

            switch ($extension) {
            	case 'jpg':
            		$img_r = imagecreatefromjpeg($src);
			        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
			        $targ_w,$targ_h,$_POST['w'],$_POST['h']);
			        imagejpeg($dst_r, $pastauser . "/thumb/" . $foto , $jpeg_quality);
	        		break;
            	
            	case 'png':
            		$img_r = imagecreatefrompng($src);
			        imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],
			        $targ_w,$targ_h,$_POST['w'],$_POST['h']);
			        imagepng($dst_r, $pastauser . "/thumb/" . $foto , $jpeg_quality);
            		break;
            }
            
			//$thumbnail = resize($targ_w,$targ_h,$pastauser."/".$foto,$pastauser . "/thumb",$foto);
	   	}

	   	/*
		/ Busca os valores de inputs tag e compara com os valores já existentes no banco
		/ $resulttags armazena outros arrays, separados por informações deletadas, alteradas, inalteradas e novas
	   	*/
	   	$showResult = false;

		if( (array_key_exists('tag', $_POST) || array_key_exists('formdata', $_POST))) {
		    // Include the autocompleteScript to know what was in the database
		    require_once ('autocompletetags.php');
		    $resulttags = array('new' => array(), 'deleted' => array(), 'changed' => array(), 'not changed' => array());
		    $tags = array_key_exists('tag', $_POST)? $_POST['tag'] : $_POST['formdata']['tags'];
		    $showResult = false;

		    foreach($tags as $key => $value) {
		        if(preg_match('/([0-9]*)-?(a|d)?$/', $key, $keyparts) === 1) {
		            $showResult = true;
		            if(isset($keyparts[2])) {
		                switch($keyparts[2]) {
		                    case 'a':
		                        if($autocompletiondata[$keyparts[1]] != $value) {
		                            // Items has changed
		                            $resulttags['changed'][] = $keyparts[1] . ' (new value: "' . $value . '")';
		                        }
		                        else {
		                            $resulttags['not changed'][] = $keyparts[1] ;
		                        }
		                        break;
		                    case 'd':
		                        $resulttags['deleted'][] = $keyparts[1] . ' ("' . $value . '")';
		                        break;
		                }
		            }
		            else {
		                $resulttags['new'][] = $value;
		            }
		        }
		    }
		}
		//caso tenhas tags diferentes das já registradas

	if(isset($resulttags)){
		if(count($resulttags['new']) > 0){
		// As tags novas são incluídas no banco
		// Depois seus cd_id são devolvidos
			$sqlInsertNewTags =  "INSERT INTO tb_tag (nm_nome) VALUES " ;
				foreach ($resulttags['new'] as $key => $value) {
					$sqlInsertNewTags .= "('". utf8_decode($value) . "')," ;
				}
			// Remove última vírgula
			$sqlInsertNewTags = substr($sqlInsertNewTags, 0, -1);
			$InsertTags = $conn->Alterar($sqlInsertNewTags);

			$sqlRetrieveNewTagsIds = "SELECT cd_id FROM tb_tag WHERE ";
				foreach ($resulttags['new'] as $key => $value) {
					$sqlRetrieveNewTagsIds .= " nm_nome = '". $value."' OR" ;
				}
			$sqlRetrieveNewTagsIds = substr($sqlRetrieveNewTagsIds, 0, -3); //Remove o AND da última inserção
		    $NewTagsIds = $conn->Buscar($sqlRetrieveNewTagsIds);
		 }
		// As tags com os nomes passados são recuperadas e armazenadas em um array
			foreach ($resulttags['not changed'] as $key => $value) {
				$tagsIds[$key] = $value;
			}

			if(count($resulttags['new']) > 0){
				foreach ($NewTagsIds as $key) {
		            $tagsIds[$key['cd_id']] = $key['cd_id'];
		        }
		    }
			$sqlInsertUserTags = "INSERT INTO tb_res_tag (cd_id_cadastro,cd_id_tag) VALUES ";
	        foreach ($tagsIds as $key => $value) {
	            $sqlInsertUserTags .= "(".$userId.",".$value ."),";
	        }
	        $sqlInsertUserTags = substr($sqlInsertUserTags, 0, -1);
	        $insereTagsUsuario = $conn->Alterar($sqlInsertUserTags);	      
	}
		
		
		/** Quando tudo é realizado, retorna a pagima de cadastro com mensagem de cadastrado realizado com sucesso */
		if(isset($foto)){
			header("Location: ../cadastro.php?e=2");
			exit;
		}else{
			header("Location: ../cadastro.php?e=3");
			exit;
		}
		
	}
	else{
		header("Location: ../cadastro.php?e=4");
		exit;
	}
	
}
else{
	header("Location: ../cadastro.php?e=1");
	exit;
}


?>