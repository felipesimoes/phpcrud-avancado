<?php 

function resize($width, $height, $original_file_path, $destino, $filename){
  /* Get original image x y*/
  list($w, $h) = getimagesize($original_file_path);
  /* calculate new image size with ratio */
  $ratio = max($width/$w, $height/$h);
  $h = ceil($height / $ratio);
  $x = ($w - $width / $ratio) / 2;
  $w = ceil($width / $ratio);
  /* new file name */
  $path = $destino . "/" . $filename ;
  /* read binary data from image file */
  $imgString = file_get_contents($original_file_path);
  /* create image from string */
  $image = imagecreatefromstring($imgString);
  $tmp = imagecreatetruecolor($width, $height);
  imagecopyresampled($tmp, $image,
    0, 0,
    $x, 0,
    $width, $height,
    $w, $h);
  /* Save image */
  switch ($_FILES['userphoto']['type']) {
    case 'image/jpeg':
      imagejpeg($tmp, $path, 100);
      break;
    case 'image/png':
      imagepng($tmp, $path, 0);
      break;
    case 'image/gif':
      imagegif($tmp, $path);
      break;
    default:
      exit;
      break;
  }
  return $path;
  /* cleanup memory */
  imagedestroy($image);
  imagedestroy($tmp);
}

function validaData($string){
  $arraydata = explode("/", $string);
  $dd = $arraydata[0];
  $mm = $arraydata[1];
  $aa = $arraydata[2];

  $dd = intval($dd);
  $mm = intval($mm);
  $aa = intval($aa);

  if( ($dd >= 1) and ($dd <= 31) ){
    if ( ($mm >= 1) and ($mm <= 12) ) {
      if ( ($aa >= 1900) and ($aa <= date("Y")) ) {
        $testaRegex = Regexteste($string);
        if ($testaRegex == true) {
          return true;
        }
        else{
          return false;
        }
      }
    }
  }
  return false;
}

function converteData($string){
    $arraydata = explode("/", $string);
    $dd = $arraydata[0];
    $mm = $arraydata[1];
    $aa = $arraydata[2];

    $dataretornada = $aa ."-". $mm ."-". $dd;
      //$stringStamp = strtotime($string);
      //$st = date("Y-m-d", $stringStamp);
    return $dataretornada;
 }
 function converteDataRecebida($string){
    $stringStamp = strtotime($string);
    $st = date("d/m/Y", $stringStamp);
    return $st;
 }

function Regexteste($string) {
  $regexe = '~^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$~';

  $comp = preg_match($regexe, $string);

  if($comp == true){
    return true;
  }
  else {
    return false;
  }
}

function rmdir_recursive($dir) {
    foreach(scandir($dir) as $file) {
        if ('.' === $file || '..' === $file) continue;
        if (is_dir("$dir/$file")) rmdir_recursive("$dir/$file");
        else unlink("$dir/$file");
    }
    rmdir($dir);
}


function noresultset($valor, $resultset, $coluna) {
    foreach ($resultset as $value) {
        if($valor == $value[$coluna]) {
            return true;
            return;
        }
   }
   return false;
}
?>