<?php
require_once "classes/conexao.class.php";
$con = new Conexao();

if(isset($_GET['e'])) $erro = $_GET['e'];

if(isset($erro)){
    switch ($erro) {
        case '1':
            echo "É preciso preencher os campos de cadastro.";
            break;
        case '2':
            echo "Usuário cadastrado com sucesso.";
            break;
        case '3':
            echo "Usuário cadastrado com sucesso. Considere enviar uma foto.";
            break;
        case '4':
            echo "É preciso preencher um email válido.";
            break;
        case '5':
            echo "É preciso que a imagem seja de extensão condizente.";
            break;
        case '6':
            echo "É preciso que a data de nascimento seja coerente.";
            break;
        case '7':
            echo "Você não passou no teste de CAPTCHA.";
            break;
    }
}

?>
<!DOCTYPE>
<html>
<meta charset="utf-8">
<title>Tela de Cadastro</title>
<head>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" href="css/jquery.Jcrop.css" type="text/css" />
    <link rel="stylesheet" href="js/chosen/chosen.css" type="text/css" />
    <link rel="stylesheet" href="js/tagedit/css/jquery.tagedit.css" type="text/css"/>
    <?php include "includes/jqueryui.php" ; ?>
    <script type="text/javascript" src="js/chosen/chosen.jquery.js"> </script>
    <script type="text/javascript" src="js/jquery.Jcrop.js"></script>  
    <script type="text/javascript" src="js/previewImg.js"></script>
    <script type="text/javascript" src="js/actions.js"></script>
    <script type="text/javascript" src="js/tagedit/js/jquery.autoGrowInput.js"></script>
    <script type="text/javascript" src="js/tagedit/js/jquery.tagedit.js"></script>
    <script type="text/javascript" src='https://www.google.com/recaptcha/api.js'></script>
</head>

<body>
<?php include "includes/menu.php"; ?>
<?php include "includes/thumb.php"; //?>

    
<h3>Novo Cadastro</h3>
    <form name="cadastro" id="cadastro_form" method="post" action="actions/cadastrar.php" action="actions/cadastrar.php" enctype="multipart/form-data"  onsubmit="return ValidarCadastro()">
        <p>Nome de usuário:</p>
        <input type="text" name="username" placeholder="Nome do usuário..." maxlength="60">
        <p>Email:</p>
        <input type="email" name="useremail" placeholder="fulano@email.com" pattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$" maxlength="60"> 
        <p>Data de nascimento:</p>
        <input type="text" id="datepicker" name="userbday" placeholder="DD/MM/AAAA" maxlength="10" pattern="^[0-3]?[0-9]\/[01]?[0-9]\/[12][90][0-9][0-9]$">
        <p>Foto (upload):</p>        
        <input type="file" id="file" accept="image/*" name="userphoto">
        <br>
        <p>Categoria:</p>
        <select data-placeholder="Escolha sua categoria..." multiple class="chosen-select" name="usercategory[]" id="usercategory" style="width:200px;">
            <?php
                $sql_tb_categoria = "SELECT nm_nome, cd_id FROM tb_categoria";
                $resul = $con->Buscar($sql_tb_categoria);

                if(!empty($resul)){
                    foreach ($resul as $res) {
                         echo "<option value='" . $res["cd_id"] .  "'>". utf8_encode($res["nm_nome"]) . "</option>";
                    }
                }
            ?>
        </select>
       
        <p>Tipos - subtipos:</p>
        <select data-placeholder="Escolha seu tipo..." multiple tab-index="-1" class="chosen-select" name="usersubtype[]" id="usersubtype" style="width:300px;">
    <?php 
        
        $sql_tb_tipo = "SELECT sub.cd_id as codigo_subtipo, sub.nm_nome as nome_subtipo, tipo.nm_nome as nome_tipo ";
        $sql_tb_tipo .= "FROM tb_subtipo sub INNER JOIN tb_tipo tipo WHERE tipo.cd_id = sub.cd_tipo";
        $resul = $con->Buscar($sql_tb_tipo);

        if(!empty($resul)){
            foreach ($resul as $res) {
                if(!isset($nm_tipo) or $res['nome_tipo'] !== $nm_tipo) {
                    $nm_tipo = $res['nome_tipo'];
                    if(!isset($nm_tipo))
                        echo "<optgroup label='".$nm_tipo."'> ";
                    else
                        echo "</optgroup>";
                        echo "<optgroup label='".$nm_tipo."'> ";
                }
                echo "<option value='" . $res["codigo_subtipo"] .  "'>". utf8_encode($res["nome_subtipo"]) . "</option>";
            }
        }
    ?>
        </select>

        <p>Tags:</p>
        <p>
        <input type="hidden" name="tag[]" value="" class="tag"/>

            
        </p>

        <input type="hidden" name="usertypeNum" id="usertypeNum" value='1'>
        <input type="hidden" id="x" name="x" />
        <input type="hidden" id="y" name="y" />
        <input type="hidden" id="w" name="w" />
        <input type="hidden" id="h" name="h" />

        <br>
        <?php include("includes/captcha.php"); ?>
        <input type="submit" value="Cadastrar">       
    </form>

	<div id="erro">
		
    </form>
	</div>

    <script>

    
    $().ready(function() {
        qtd = $("#usertypeNum").val();
        // gerenciarTipos("<?php if (isset($_POST['tipos'])) echo $_SERVER['QUERY_STRING']; else echo 'qtd='; ?>"+qtd); 

       var config = {
          '.chosen-select'           : {},
          '.chosen-select-deselect'  : {allow_single_deselect:true},
          '.chosen-select-no-single' : {disable_search_threshold:10},
          '.chosen-select-no-results': {no_results_text:'Desculpe, nenhuma opção encontrada!'},
          '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
          $(selector).chosen(config[selector]);
        }
       
    });
    $("#cadastro_form").find('input.tag').tagedit({
        //autocompleteURL: 'js/tagedit/server/autocomplete.php',
        autocompleteURL: 'actions/autocompletetags.php'
    });
    </script>
    

</body>
</html>