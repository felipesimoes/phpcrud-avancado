<?php
require_once "classes/conexao.class.php";
require_once "sys/functions.php";
$con = new Conexao();

if(isset($_GET['e'])) $erro = $_GET['e'];

if(isset($erro)){
    switch ($erro) {
        case '1':
            $erro = "Usuario nao identificado.";
            break;
        case '2':
            $erro = "Usuario notificado.";
            break;
        case '3':
            $erro = "Usuario notificado.";
            break;
        case '4':
            $erro = "Usuario não pode ser excluido.";
            break;
        case '5':
            $erro = "Usuario excluido corretamente.";
            break;
        case '6':
            $erro = "Não existe usuário para ser excluido.";
            break;
    }
}

?>
<!DOCTYPE html>
<html lang="pt-br">
<meta charset="utf-8">
<head>
	<meta charset="UTF-8">
	<title>Listagem e busca</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script> 
    <script type="text/javascript" src="js/pesquisa.js"></script>
    <script type="text/javascript" src="js/table-sorter/jquery.tablesorter.js"></script>
     <script type="text/javascript" src="js/tableexport/tableExport.js"> </script>
    <script type="text/javascript" src="js/tableexport/jquery.base64.js"></script>
    <script type="text/javascript" src="js/tableexport/jspdf/libs/sprintf.js"></script>
    <script type="text/javascript" src="js/tableexport/jspdf/jspdf.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <link rel="stylesheet" type="text/css" href="css/blue-table-sorter/style-table-sorter.css" />
    <style>
    table,td {
    	border: 1px solid black;
    	text-align: center;
    	margin-top: 30px;
    	margin-left: 20px;
    }
    td {
    	padding-left: 20px;
    	padding-right: 20px;
    }
    
    </style>
</head>
<body>
    <div id="notificacao"><strong><?php if(isset($erro)) echo $erro; ?></strong></div>
	<?php include("includes/menu.php"); ?>

	<form action="" method="GET" id="pesquisaForm" name="pesquisa">
		<input type="text" name="pesquisa" id="mainPesquisa" placeholder="Digite o nome para buscar...">
        <input type="hidden" name="categoria" value="nm_nome" id="categoria">
	</form>
	

	<div id="resultado">
		
	</div>
	 <script>
	    $().ready(function() {
            var cat = $("#categoria").val();
	       pesquisaAjax("<?php if (isset($_GET['pesquisa'])) echo $_SERVER['QUERY_STRING']; else echo 'pesquisa='; ?>", 'nm_nome'); 
	       $("#myTable").tablesorter(); 
        });

    </script>
</body>
</html>